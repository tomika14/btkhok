<?php
require_once ("include/config.php");
include ("include/functions.php");
$getid = $_GET["id"];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PTE BTK | HÖK</title>
    <link rel="shortcut icon" href="pic/favicon.ico" type="image/x-icon">
    <link rel="icon" href="pic/favicon.ico" type="image/x-icon">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" />
    <script src="vendor/jquery/jquery.slim.min.js"></script>
</head>

<body style="background-repeat: no-repeat">
<nav class="navbar navbar-expand-lg navbar-light bg-light static-top navbox">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="pic/theme_default_logo.png"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active menuactive">
                    <a class="nav-link" href="index.php">Kezdőlap
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hok.php">HÖK</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Szenes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">PB Magazin</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Gólyáknak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kapcsolat</a>
                </li>
                <li class="nav-item">
                    <div class="dropdown">
                        <button class="btn btn-info dropdown-toggle dropdownbtn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menü
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">HÖK Tagok</a>
                            <a class="dropdown-item" href="#">Második almenü</a>
                            <a class="dropdown-item" href="#">Harmadik almenü</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12 contbox">
            <?php
            //addHomeNews();
            listOneNews($getid);
            ?>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <span class="text-muted">CopyRight</span>
    </div>
</footer>
<div style="clear:both"></div>
<script src="vendor/jquery/jquery.slim.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


</body>

</html>
