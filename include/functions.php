<?php
require_once ('config.php');

function listNews(){
    $conn = new mysqli(HOST,USER,PASS,DB);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT btkhok_news.id AS newsid, title,content_small,ndate,full_name FROM btkhok_news, users WHERE authorId = users.id ORDER BY ndate DESC LIMIT 5";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {
            echo "<table class='table table-borderless'>";
            echo "<tr>";
            echo "<th scope='col'>";
            echo "$row[title]";
            echo "<hr class='m-0 w-25 fade' />";
            echo "</th>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "$row[content_small]...";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "$row[ndate]";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "<i>$row[full_name]</i>";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "<a href='news.php?id=$row[newsid]'>Több olvasása</a>";
            echo "<hr class='mt-1 mb-1' />";
            echo "</table>";
        }
    } else {
        echo "Nincs hír!";
    }
    $conn->close();
}
function listOneNews($getid){

    $conn = new mysqli(HOST,USER,PASS,DB);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT btkhok_news.id, title,content,ndate,full_name FROM btkhok_news, users WHERE authorId = users.id AND btkhok_news.id = '$getid'";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "<table class='table table-borderless'>";
            echo "<tr>";
            echo "<th scope='col'>";
            echo "<h3 class='mt-5'>$row[title]</h3>";
            echo "<hr />";
            echo "</th>";
            echo "</tr>";
            echo "<tr>";
            echo "<td style='border-top: 1px solid gray'>";
            echo nl2br($row["content"]);
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td style='border-top: 1px solid gray'>";
            echo "Dátum: $row[ndate]";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "Szerző: <i>$row[full_name]</i>";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "<a href='./index.php'>Vissza a kezdőlapra</a>";
            echo "</table>";

        }
    } else {
        echo "Nincs hír!";
    }
    $conn->close();


}
function adminList(){
    $conn = new mysqli(HOST,USER,PASS,DB);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT full_name,pic,rank,email,tel FROM users";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "<div class='tagok'>";
            echo "<img src='$row[pic]' alt='$row[full_name]'/>";
            echo "<div class='alairas'>";
            echo "<h4>$row[full_name]</h4>";
            echo "<p>$row[rank]</p>";
            echo "<p>$row[email]</p>";
            echo "<p>$row[tel]</p>";
            echo "</div>";
            echo "</div>";
        }
    } else {
        echo "Nincs admin!";
    }
    $conn->close();
}
function allNews(){
    $conn = new mysqli(HOST,USER,PASS,DB);

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT btkhok_news.id AS newsid, title,content_small,ndate,full_name FROM btkhok_news, users WHERE authorId = users.id ORDER BY ndate DESC";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
            //echo "<form action='news.php'>";
            echo "<table class='table table-borderless'>";
            echo "<tr>";
            echo "<th scope='col'>";
            echo "$row[title]";
            echo "<hr class='m-0 w-25 fade' />";
            echo "</th>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "$row[content_small]...";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "$row[ndate]";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "<i>$row[full_name]</i>";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            //echo "<input type='hidden' id='id' name='id' value='$row[newsid]' />";
            //echo "<input type='submit' value='Több..'>";
            echo "<a href='news.php?id=$row[newsid]'>Több olvasása</a>";
            echo "<hr class='mt-1 mb-1' />";
            echo "</table>";

            //echo "</form>";

        }
    } else {
        echo "Nincs hír!";
    }
    $conn->close();
}
function calcNews(){
    $conn = new mysqli(HOST,USER,PASS,DB);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT btkhok_news.id AS newsid, title,content_small,ndate,full_name FROM btkhok_news, users WHERE authorId = users.id ORDER BY ndate DESC";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "<br /><a href='all.php' class='text-light'>Több hír olvasása...</a>";
        }
    }
    $conn->close();
}
?>