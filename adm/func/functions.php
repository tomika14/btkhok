<?php

require_once "cfg.php";


function listNews(){
    $conn = new mysqli(HOST,USER,PASS,DB);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT btkhok_news.id AS newsid, title,content_small,ndate,full_name FROM btkhok_news, users WHERE authorId = users.id ORDER BY ndate DESC";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {

            echo "<table class='news'>";
            echo "<tr>";
            echo "<th>";
            echo "ID:";
            echo "</th>";
            echo "<th>";
            echo "Cím:";
            echo "</th>";
            echo "<th>";
            echo "Szöveg";
            echo "</th>";
            echo "<th>";
            echo "Dátum";
            echo "</th>";
            echo "<th>";
            echo "Szerkesztő:";
            echo "</th>";
            echo "<th>";
            echo "Módosítás";
            echo "</th>";
            echo "<th>";
            echo "Törlés";
            echo "</th>";
            echo "<tr>";
            echo "<td>";
            echo "$row[newsid]";
            echo "</td>";
            echo "<td>";
            echo "$row[title]";
            echo "</td>";
            echo "<td>";
            echo "$row[content_small]";
            echo "</td>";
            echo "<td>";
            echo "$row[ndate]";
            echo "</td>";
            echo "<td>";
            echo "$row[full_name]";
            echo "</td>";
            echo "<td>";
            echo "<a href='modnews.php?id=$row[newsid]'>Módosítás</a>";
            echo "</td>";
            echo "<td>";
            echo "<a href='delnews.php?id=$row[newsid]'>Törlés</a>";
            echo "</td>";
            echo "</tr>";
            echo "</table>";

        }
    } else {
        echo "Nincs hír!";
    }
    $conn->close();
}
function listUsers(){
    $conn = new mysqli(HOST,USER,PASS,DB);


    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT id,username,email,full_name,create_date,rank,tel,admin FROM users";
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {

            echo "<table class='news'>";
            echo "<tr>";
            echo "<th>";
            echo "ID:";
            echo "</th>";
            echo "<th>";
            echo "Felhasználónév:";
            echo "</th>";
            echo "<th>";
            echo "Email";
            echo "</th>";
            echo "<th>";
            echo "Teljes név:";
            echo "</th>";
            echo "<th>";
            echo "Dátum:";
            echo "</th>";
            echo "<th>";
            echo "Pozíció:";
            echo "</th>";
            echo "<th>";
            echo "Telefonszám";
            echo "</th>";
            echo "<th>";
            echo "Törlés";
            echo "</th>";
            echo "<th>";
            echo "Módosítás";
            echo "</th>";
            echo "<tr>";
            echo "<td>";
            echo "$row[id]";
            echo "</td>";
            echo "<td>";
            echo "$row[username]";
            echo "</td>";
            echo "<td>";
            echo "$row[email]";
            echo "</td>";
            echo "<td>";
            echo "$row[full_name]";
            echo "</td>";
            echo "<td>";
            echo "$row[create_date]";
            echo "</td>";
            echo "<td>";
            echo "$row[rank]";
            echo "</td>";
            echo "<td>";
            echo "$row[tel]";
            echo "</td>";
            echo "<td>";
            echo "<a href='userdel.php?id=$row[id]'>Törlés</a>";
            echo "</td>";
            echo "<td>";
            echo "<a href='#'>Módosítás</a>";
            echo "</td>";
            echo "</tr>";
            echo "</table>";
        }
    }
    $conn->close();
}
function modNews($getid){
    $conn = new mysqli(HOST,USER,PASS,DB);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $id = $getid;
    $title = $_POST["title"];
    $content = $_POST["content"];
    $cont_sm = str_split($content,100);
    $content_small = $cont_sm[0];
    $authorid = $_SESSION['id'];
    $datum = $date("Y-m-d");

    $sql = "UPDATE btkhok_news SET title = '$title', content = '$content', content_small = '$content_small', authorId = '$authorid', ndate = '$datum' WHERE id ='$id'";
    if(mysqli_query($conn, $sql)){
        echo "Records added successfully.";
        header('Location: listnews.php');
    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
    $conn->close();
}
?>
