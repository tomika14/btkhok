<?php

require_once ("cfg.php");
session_start();
$con = mysqli_connect(HOST, USER, PASS, DB);
if ( mysqli_connect_errno() ) {
    exit('Failed to connect to MySQL: ' . mysqli_connect_error());
}
if ( !isset($_POST['username'], $_POST['password']) ) {
    exit('Kérlek töltsd ki a felhasználóneved és a jelszavad is!');
}
if ($stmt = $con->prepare('SELECT id, password,admin FROM users WHERE username = ?')) {
    $stmt->bind_param('s', $_POST['username']);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        $stmt->bind_result($id, $password, $admin);
        $stmt->fetch();

        if (password_verify($_POST['password'], $password)) {
            session_regenerate_id();
            $_SESSION['loggedin'] = TRUE;
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['id'] = $id;
            $_SESSION['admin'] = $admin;
            echo 'Üdv ' . $_SESSION['username'] . '!';
            echo "<br /><a href='../index.php'>Tovább a kezdőlapra!</a>";
            header ("../index.php");
        } else {
            echo 'Incorrect username and/or password!';
        }
    } else {
        echo 'Incorrect username and/or password!';
    }
    $stmt->close();
}
?>
