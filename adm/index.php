<?php
include "auth.php";
require_once ("func/cfg.php");
$admin = $_SESSION["admin"];

$conn = new mysqli(HOST,USER,PASS,DB);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$uname = $_SESSION['username'];
$sql = "SELECT full_name FROM users WHERE username = '$uname'";
$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
}
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
       $fullname = $row["full_name"];

    }
} else {
    echo "Nincs hír!";
}
$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BTK HÖK ADMIN</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>
<body class="loggedin">
<nav class="navtop">
    <div>
        <h1><a href="index.php">BTK HÖK | ADMIN</a></h1>
        <a href="profile.php"><i class="fas fa-user-circle"></i>Profil</a>
        <a href="logout.php"><i class="fas fa-sign-out-alt"></i>Kijelentkezés</a>

    </div>
</nav><?php
$admin = $_SESSION["admin"];
if($admin === 1){
    ?>
    <nav class="navtop">
        <div>
            <a href="addnews.php"><i class="fa fa-newspaper"></i>Új hír hozzáadás</a>
            <a href="addadmin.php"><i class="fa fa-user-circle"></i>Új tag hozzáadás</a>
            <a href="listnews.php"><i class="fa fa-newspaper"></i>Hírek listája</a>
            <a href="listusers.php"><i class="fa fa-user-circle"></i>Tagok listája</a>
        </div>
    </nav>
    <?php
}
?>
<div class="content">
    <h2>Kezdőlap</h2>
    <p>Üdv <?php echo $fullname; ?>!</p>
</div>
</body>
</html>
