<?php
require_once ("func/cfg.php");
//include ("func/functions.php");
include "auth.php";
include "adminauth.php";
$getid = $_GET["id"];

$conn = new mysqli(HOST,USER,PASS,DB);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT title,content FROM btkhok_news WHERE id = '$getid'";
$result = $conn->query($sql);
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
}
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $title = $row["title"];
        $content = $row["content"];

    }
}
$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BTK HÖK ADMIN</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>
<body class="loggedin">
<nav class="navtop">
    <div>
        <h1><a href="index.php">BTK HÖK | ADMIN</a></h1>
        <a href="profile.php"><i class="fas fa-user-circle"></i>Profil</a>
        <a href="logout.php"><i class="fas fa-sign-out-alt"></i>Kijelentkezés</a>

    </div>
</nav><?php
$admin = $_SESSION["admin"];
if($admin === 1){
    ?>
    <nav class="navtop">
        <div>
            <a href="addnews.php"><i class="fa fa-newspaper"></i>Új hír hozzáadás</a>
            <a href="addadmin.php"><i class="fa fa-user-circle"></i>Új tag hozzáadás</a>
            <a href="listnews.php"><i class="fa fa-newspaper"></i>Hírek listája</a>
            <a href="listusers.php"><i class="fa fa-user-circle"></i>Tagok listája</a>
        </div>
    </nav>
    <?php
}
?>
<div class="content">
    <h2>Hír módosítása</h2>
    <div>
        <form method="post" action="func/modnews.php">
            <input type="text" name="title" value="<?=$title?>" /><br />
            <textarea name="content" rows="20" cols="80"><?=$content?></textarea><br />
            <input type="hidden" id="id" name="id" value=<?=$getid?>>
            <input type="submit" name="submit" value="Mentés">
        </form>
    </div>
</div>

</body>
</html>
