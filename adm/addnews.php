<?php
include "auth.php";
require_once "func/cfg.php";
include "adminauth.php";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $conn = new mysqli(HOST, USER, PASS, DB);

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $title = $_POST["title"];
    $authorId = $_SESSION["id"];
    $content = $_POST["content"];
    $cont_sm = str_split($content, 150);
    $content_small = $cont_sm[0];
    //$ndate = date("Y F d h:i:s");
    $sql = "INSERT INTO btkhok_news (title,authorId,content,content_small) VALUES ('$title','$authorId','$content','$content_small')";
    if (mysqli_query($conn, $sql)) {
        header("Location: addnews.php");
    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BTK HÖK ADMIN</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>
<body class="loggedin">
<nav class="navtop">
    <div>
        <h1><a href="index.php">BTK HÖK | ADMIN</a></h1>
        <a href="profile.php"><i class="fas fa-user-circle"></i>Profil</a>
        <a href="logout.php"><i class="fas fa-sign-out-alt"></i>Kijelentkezés</a>

    </div>
</nav><?php
$admin = $_SESSION["admin"];
if($admin === 1){
    ?>
    <nav class="navtop">
        <div>
            <a href="addnews.php"><i class="fa fa-newspaper"></i>Új hír hozzáadás</a>
            <a href="addadmin.php"><i class="fa fa-user-circle"></i>Új tag hozzáadás</a>
            <a href="listnews.php"><i class="fa fa-newspaper"></i>Hírek listája</a>
            <a href="listusers.php"><i class="fa fa-user-circle"></i>Tagok listája</a>
        </div>
    </nav>
    <?php
}
?>
<div class="content">
    <h2>Hír hozzáadása</h2>
    <div>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label for="title">Cím</label><br />
        <input type="text" name="title" placeholder="Cím" /><br />
        <label for="content">Szöveg:</label><br />
        <textarea name="content" rows="20" cols="80"></textarea><br />
        <input type="submit" name="submit" value="Küldés">
    </form>
    </div>
</div>
</body>
</html>
