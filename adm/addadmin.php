<?php
include "auth.php";
include "adminauth.php";
require_once "func/cfg.php";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $conn = new mysqli(HOST, USER, PASS, DB);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $fullname = $_POST["full_name"];
    $username = $_POST["username"];
    $password_string = $_POST["password"];
    $password = password_hash($password_string, PASSWORD_DEFAULT);
    $email = $_POST["email"];
    $rank = $_POST["beoszt"];
    $tel = $_POST["tel"];
    $adm = $_POST["admin"];
    if(!empty($fullname) && !empty($username) && !empty($password) && !empty($email) && !empty($rank) && !empty($tel)){
    $sql = "INSERT INTO users (username,password,email,full_name,rank,tel,admin) VALUES ('$username','$password','$email','$fullname','$rank','$tel','$adm')";

    if (mysqli_query($conn, $sql)) {
        header("Location: addadmin.php");
    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
    }else{
        echo "Mindent ki kell tölteni!";
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BTK HÖK ADMIN</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>
<body class="loggedin">
<nav class="navtop">
    <div>
        <h1><a href="index.php">BTK HÖK | ADMIN</a></h1>
        <a href="profile.php"><i class="fas fa-user-circle"></i>Profil</a>
        <a href="logout.php"><i class="fas fa-sign-out-alt"></i>Kijelentkezés</a>

    </div>
</nav><?php
$admin = $_SESSION["admin"];
if($admin === 1){
    ?>
    <nav class="navtop">
        <div>
            <a href="addnews.php"><i class="fa fa-newspaper"></i>Új hír hozzáadás</a>
            <a href="addadmin.php"><i class="fa fa-user-circle"></i>Új tag hozzáadás</a>
            <a href="listnews.php"><i class="fa fa-newspaper"></i>Hírek listája</a>
            <a href="listusers.php"><i class="fa fa-user-circle"></i>Tagok listája</a>
        </div>
    </nav>
    <?php
}
?>
<div class="content">
    <h2>Admin hozzáadása</h2>
    <div>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label for="full_name">Teljes név:</label>
        <input type="text" name="full_name" placeholder="Teljes név" /><br />
        <input type="text" name="username" placeholder="Felhasználónév" /><br />
        <input type="password" name="password" placeholder="Jelszó" /><br />
        <input type="email" name="email" placeholder="Email" /><br />
        <input type="text" name="beoszt" placeholder="Beosztás" /><br />
        <input type="tel" name="tel" placeholder="+36001234567" /><br />
        <input type="radio" id="tag" name="admin" value="0">
        <label for="tag">Csak tag</label><br />
        <input type="radio" id="admin" name="admin" value="1">
        <label for="admin">Admin</label><br />
        <input type="submit" name="submit" value="Küldés">

    </form>
    </div>
</div>
</body>
</html>
