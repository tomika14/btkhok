<?php
require_once ("func/cfg.php");
include "auth.php";
if (!isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit;
}
$con = mysqli_connect(HOST, USER, PASS, DB);
if (mysqli_connect_errno()) {
    exit('Failed to connect to MySQL: ' . mysqli_connect_error());
}
$stmt = $con->prepare('SELECT full_name, email, pic FROM users WHERE id = ?');

$stmt->bind_param('i', $_SESSION['id']);
$stmt->execute();
$stmt->bind_result($full_name, $email, $pic);
$stmt->fetch();
$stmt->close();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BTK HÖK ADMIN</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>
<body class="loggedin">
<nav class="navtop">
    <div>
        <h1><a href="index.php">BTK HÖK | ADMIN</a></h1>
        <a href="profile.php"><i class="fas fa-user-circle"></i>Profil</a>
        <a href="logout.php"><i class="fas fa-sign-out-alt"></i>Kijelentkezés</a>

    </div>
</nav><?php
$admin = $_SESSION["admin"];
if($admin === 1){
    ?>
    <nav class="navtop">
        <div>
            <a href="addnews.php"><i class="fa fa-newspaper"></i>Új hír hozzáadás</a>
            <a href="addadmin.php"><i class="fa fa-user-circle"></i>Új tag hozzáadás</a>
            <a href="listnews.php"><i class="fa fa-newspaper"></i>Hírek listája</a>
            <a href="listusers.php"><i class="fa fa-user-circle"></i>Tagok listája</a>
        </div>
    </nav>
    <?php
}
?>
<div class="content">
    <h2>Profil</h2>
    <div>
        <p>A Profilod adatai:</p>
        <table>
            <tr>
                <td>Felhasználónév:</td>
                <td><?=$_SESSION['username']?></td>
            </tr>
            <tr>
                <td>Teljes név:</td>
                <td><?=$full_name?></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><?=$email?></td>
            </tr>
            <?php
            if($pic === "pic/upload/noavatar.png"){
            ?>
            <tr>
                <td>
                    <form action="upload.php" method="post" enctype="multipart/form-data">
                        Kép feltöltés:
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <input type="submit" value="Upload Image" name="submit">
                    </form>
                </td>
            </tr>
            <?php
            }
            else{
            ?>
            <tr>
                <td>
                    Profil kép: <img src="../<?=$pic?>" class="profimg" />
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>

</body>
</html>
