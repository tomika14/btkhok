<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Bejelentkezés</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 16px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; margin: 0 auto; }
    </style>
</head>
<body>
<div class="wrapper">
    <h2>Bejelentkezés</h2>
    <!--<form action="" method="post">-->
        <form action="func/login.php" method="post">
        <div class="form-group">
            <label>Felhasználónév</label>
            <input type="text" name="username" class="form-control" value="">
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <label>Jelszó</label>
            <input type="password" name="password" class="form-control">
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Bejelentkezés">
        </div>
    </form>
</div>
</body>
</html>