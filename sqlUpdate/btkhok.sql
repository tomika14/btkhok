-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Feb 27. 16:59
-- Kiszolgáló verziója: 10.4.14-MariaDB
-- PHP verzió: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `btkhok`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `btkhok_news`
--

CREATE TABLE `btkhok_news` (
  `id` int(20) NOT NULL,
  `title` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `authorId` int(50) NOT NULL,
  `content` text COLLATE utf8_hungarian_ci NOT NULL,
  `content_small` text COLLATE utf8_hungarian_ci NOT NULL,
  `ndate` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `btkhok_news`
--

INSERT INTO `btkhok_news` (`id`, `title`, `authorId`, `content`, `content_small`, `ndate`) VALUES
(1, 'Teszt hír2', 2, 'Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum ', 'Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lorem ipsum lorem ipsum Lore', '2021-02-27');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `full_name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `create_date` date NOT NULL DEFAULT current_timestamp(),
  `pic` varchar(255) COLLATE utf8_hungarian_ci DEFAULT 'pic/upload/noavatar.png',
  `rank` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `admin` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `full_name`, `create_date`, `pic`, `rank`, `tel`, `admin`) VALUES
(2, 'admin', '$2y$10$DoIaatolgMmORBSA.RkeVuspBqYls2rpZNTCOk5p5pXmM5xSCgKc6', 'takacst7200@gmail.com', 'Takács Tamás', '2021-02-21', 'pic/upload/admin.jpg', 'Admin', '+36703693712', 1),
(6, 'dorina', '$2y$10$VHxTTeCsQf7FwHRgIcVYxuomHucZay10z7WMs54p5Dq5cekWf4Iya', 'dorina.bogyo.98@gmail.com', 'Bogyó Dorina', '2021-02-27', 'pic/upload/noavatar.png', 'Teszt', '+36301231234', 0);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `btkhok_news`
--
ALTER TABLE `btkhok_news`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `btkhok_news`
--
ALTER TABLE `btkhok_news`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
